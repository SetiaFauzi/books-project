package com.example.book.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.ModelDTO.AuthorDTO;
import com.example.book.ModelDTO.RatingDTO;
import com.example.book.exception.ResourceNotFoundException;
import com.example.book.model.Author;
import com.example.book.model.Rating;
import com.example.book.repositories.RatingRepository;

@RestController
@RequestMapping("/api")
public class RatingController {

	@Autowired
	RatingRepository ratingRepository;
	
	
	@GetMapping("/rating/read")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
	
		ArrayList<Rating> listRatingEntity = (ArrayList<Rating>) ratingRepository.findAll();
		ArrayList<RatingDTO> listRatingDTO = new ArrayList<RatingDTO>();
		
		for(Rating rating : listRatingEntity) {
			RatingDTO ratingDTO = modelMapper.map(rating, RatingDTO.class);
			listRatingDTO.add(ratingDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read all Rating data succes");
		result.put("Data", listRatingDTO);
		
		return result;
		
	}
	
	@PostMapping("/rating/create")
	public HashMap<String, Object> createRatingDTOMaping(@Valid @RequestBody RatingDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Rating ratingEntity = modelMapper.map(body, Rating.class);
		
		ratingRepository.save(ratingEntity);
		
		result.put("Status", 200);
		result.put("Message", "Create New Rating Succes");
		result.put("Data", body);
		
		return result;
	
		
	}
	
	@PutMapping("/rating/update/{id}")
	public HashMap<String, Object> updateRatingDTOMaping(@PathVariable(value = "id") Long ratingID,
            @Valid @RequestBody RatingDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Rating ratingEntity = ratingRepository.findById(ratingID)
				.orElseThrow(() -> new ResourceNotFoundException ("Rating", "id", ratingID));
		
		ratingEntity = modelMapper.map(body, Rating.class);
		ratingEntity.setRatingID(ratingID);
		
		ratingRepository.save(ratingEntity);
		body = modelMapper.map(ratingEntity, RatingDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Rating Succes");
		result.put("Data", body);
		
		return result;
		

	}
	
	@DeleteMapping("/rating/delete/{id}")
	public HashMap<String, Object> deleteRatingDTOMaping(@PathVariable(value = "id") Long ratingID,
            RatingDTO ratingDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Rating ratingEntity = ratingRepository.findById(ratingID)
				.orElseThrow(() -> new ResourceNotFoundException ("Rating", "id", ratingID));
		
		ratingEntity = modelMapper.map(ratingDTO, Rating.class);
		ratingEntity.setRatingID(ratingID);
		
		ratingRepository.delete(ratingEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete One of Rating Succes");
		
		return result;
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
