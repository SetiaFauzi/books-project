package com.example.book.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.ModelDTO.PaperDTO;
import com.example.book.ModelDTO.PublisherDTO;
import com.example.book.exception.ResourceNotFoundException;
import com.example.book.model.Paper;
import com.example.book.model.Publisher;
import com.example.book.repositories.AuthorRepository;
import com.example.book.repositories.PublisherRepository;

import net.bytebuddy.asm.Advice.Return;



@RestController
@RequestMapping("/api")
public class PublisherController {
	
	@Autowired
	PublisherRepository publisherRepository;
	
	
	@GetMapping("/publisher/readAll")
	public List<Publisher>getAllPublishers(){
		return publisherRepository.findAll();
		
	}
	
	@GetMapping("/publisher/readDTO")
	public HashMap<String, Object>getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();

		//Ambil semua data dari publisher dg menggunakan method find all dr repos
		ArrayList<Publisher> listPublisherEntity = (ArrayList<Publisher>) publisherRepository.findAll();
		
		//Buat sebuah arraylistDTO
		ArrayList<PublisherDTO> listPublisherDTO = new ArrayList<PublisherDTO>();
		
		//Maping semua object dari entity ke DTO menggunakan Loooping
		for(Publisher publisher : listPublisherEntity) {
			//Inisailisai DTO
			PublisherDTO publisherDTO = new PublisherDTO();
			//inis obj paper dto
			PaperDTO paperDTO = new PaperDTO();
			
			//Maping nilai dr entity ke dto
			
			paperDTO.setPaperID(publisher.getPaperQuality().getPaperID());
			paperDTO.setPaperPrice(publisher.getPaperQuality().getPaperPrice());
			paperDTO.setQualityName(publisher.getPaperQuality().getQualityName());
			
			//Maping nilai dr entity kr DTO
			
			publisherDTO.setPublisheID(publisher.getPublisheID());
			publisherDTO.setCompanyName(publisher.getCompanyName());
			publisherDTO.setCountry(publisher.getCountry());
			publisherDTO.setPaperQuality(paperDTO);
			
			//memasukan object publisher DTO ke arraylist
			listPublisherDTO.add(publisherDTO);
			
			
		}
		
		result.put("Status", 200);
		result.put("Message", "Read all publisher data succes");
		result.put("Data", listPublisherDTO);
		
		return result;
	}
	
	@GetMapping("/publisher/readDTOMaping")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
	
		ArrayList<Publisher> listPublisherEntity = (ArrayList<Publisher>) publisherRepository.findAll();
		ArrayList<PublisherDTO> listPublisherDTO = new ArrayList<PublisherDTO>();
		
		for(Publisher publisher : listPublisherEntity) {
			PublisherDTO publisherDTO = modelMapper.map(publisher, PublisherDTO.class);
			listPublisherDTO.add(publisherDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read all publisher data succes");
		result.put("Data", listPublisherDTO);
		
		return result;
		
	}
	
	

	
//	@PostMapping("/publisher/create")
//	public Publisher createPublisher(@Valid @RequestBody Publisher publisher) {
//		return publisherRepository.save(publisher);
//	}
	
	@PostMapping("/publisher/createDTO")
	public HashMap<String, Object> createPublisherDTO(@Valid @RequestBody PublisherDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//buat Sebuah Object Publisher entity
		Publisher publisherEntity = new Publisher();
		
		//buat Sebuah Object Publisher entity(krna butuh untuk setPaperQuality)
		Paper paperEntity = new Paper();
		
		//inisialisasi obj paper entity
		paperEntity.setPaperID(body.getPaperQuality().getPaperID());
		
		
		//Maping value dari publisher DTO ke entity
		publisherEntity.setCompanyName(body.getCompanyName());
		publisherEntity.setCountry(body.getCountry());
		publisherEntity.setPaperQuality(paperEntity);
	
		//ini save data ke database
		publisherRepository.save(publisherEntity);
		
		body.setPublisheID(publisherEntity.getPublisheID());
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Create New Publisher Succes");
		result.put("Data", body);
				
				
		return result;
	}
	
	@PostMapping("/publisher/create")
	public HashMap<String, Object> createPublisherDTOMaping(@Valid @RequestBody PublisherDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
	
		ModelMapper modelMapper = new ModelMapper();
		Publisher publisherEntity = modelMapper.map(body, Publisher.class);
		publisherRepository.save(publisherEntity);
		
		result.put("Status", 200);
		result.put("Message", "Create New Publisher Succes");
		result.put("Data", body);
		
		return result;
		
	}
	

	
	@GetMapping("/publisher/{id}")
	public Publisher getPublisherById(@PathVariable(value = "id") Long publisherId) {
		
		return publisherRepository.findById(publisherId)
				.orElseThrow(() -> new ResourceNotFoundException ("Publisher", "id", publisherId));
		
	}
	
//	@PutMapping("/publisher/update/{id}")
//	public Publisher updatePublisher(@PathVariable(value = "id") Long publisherId,
//            @Valid @RequestBody Publisher publisherDetails) {
//		HashMap<String, Object> result = new HashMap<String, Object>();
//		
//		
//		Publisher publisher = publisherRepository.findById(publisherId)
//				.orElseThrow(() -> new ResourceNotFoundException ("Publisher", "id", publisherId));
//		
//		publisher.setCompanyName(publisherDetails.getCompanyName());
//		publisher.setCountry(publisherDetails.getCountry());
//		publisher.setPaperQuality(publisherDetails.getPaperQuality());
//
//		Publisher updatedPublisher = publisherRepository.save(publisher);
//		
//		//Maping result untuk respon API
//		result.put("Status", 200);
//		result.put("Message", "Update one of a Books is Succes");
//		result.put("Data", publisherDetails);
//				
//		return updatedPublisher;
//		
//		
//	}
	
	
	@PutMapping("/publisher/updateDTO/{id}")
	public HashMap<String, Object> updatePublisherDTO(@PathVariable(value = "id") Long publisherId,
            @Valid @RequestBody PublisherDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//buat Sebuah Object Publisher entity
		Publisher publisherEntity = publisherRepository.findById(publisherId)
				.orElseThrow(() -> new ResourceNotFoundException ("Publisher", "id", publisherId));
		
		//buat Sebuah Object Publisher entity(krna butuh untuk setPaperQuality)
		Paper paperEntity = new Paper();
		
		//inisialisasi obj paper entity
		paperEntity.setPaperID(body.getPaperQuality().getPaperID());
		
		
		//Maping value dari publisher DTO ke entity
		publisherEntity.setCompanyName(body.getCompanyName());
		publisherEntity.setCountry(body.getCountry());
		publisherEntity.setPaperQuality(paperEntity);
	
		//ini save data ke database
		publisherRepository.save(publisherEntity);
		
		body.setPublisheID(publisherEntity.getPublisheID());
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Update New Publisher Succes");
		result.put("Data", body);
				
				
		return result;
	}
	
	@PutMapping("/publisher/update/{id}")
	public HashMap<String, Object> updatePublisherDTOMaping(@PathVariable(value = "id") Long publisherId,
            @Valid @RequestBody PublisherDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Publisher publisherEntity = publisherRepository.findById(publisherId)
				.orElseThrow(() -> new ResourceNotFoundException ("Publisher", "id", publisherId));
		
		
		publisherEntity = modelMapper.map(body, Publisher.class);
		publisherEntity.setPublisheID(publisherId);
		
		publisherRepository.save(publisherEntity);
		body = modelMapper.map(publisherEntity, PublisherDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update New Publisher Succes");
		result.put("Data", body);
		
		return result;
		
	}

	
	
	
	
	
//	
//	@DeleteMapping("/publisher/delete/{id}")
//	public ResponseEntity<?> deletePublisher(@PathVariable(value = "id") Long publisherId){
//		
//		Publisher publisher = publisherRepository.findById(publisherId)
//				.orElseThrow(() -> new ResourceNotFoundException ("Publisher", "id", publisherId));
//		
//		publisherRepository.delete(publisher);
//		return ResponseEntity.ok().build();
//		
//		
//		
//	}
	
	
	@DeleteMapping("/publisher/deleteDTO/{id}")
	public HashMap<String, Object> deletePublisherDTO(@PathVariable(value = "id") Long publisherId,
           PublisherDTO publisherDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//buat Sebuah Object Publisher entity
		Publisher publisherEntity = publisherRepository.findById(publisherId)
				.orElseThrow(() -> new ResourceNotFoundException ("Publisher", "id", publisherId));
		
		//buat Sebuah Object Publisher entity(krna butuh untuk setPaperQuality)
		Paper paperEntity = new Paper();
		
		//inisialisasi obj paper entity
		//paperEntity.setPaperID(publisherDTO.getPaperQuality().getPaperID());
		
		//Maping value dari publisher DTO ke entity
		publisherEntity.setCompanyName(publisherDTO.getCompanyName());
		publisherEntity.setCountry(publisherDTO.getCountry());
		publisherEntity.setPaperQuality(paperEntity);
		
		//Proses Delete
		publisherRepository.delete(publisherEntity);
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Delete Publisher Succes");
		
		
		return result;
		
		
		
	}
	
	@DeleteMapping("/publisher/delete/{id}")
	public HashMap<String, Object> deletePublisherDTOMaping(@PathVariable(value = "id") Long publisherId,
	           PublisherDTO publisherDTO){
			HashMap<String, Object> result = new HashMap<String, Object>();
			ModelMapper modelMapper = new ModelMapper();
			
	Publisher publisherEntity = publisherRepository.findById(publisherId)
			.orElseThrow(() -> new ResourceNotFoundException ("Publisher", "id", publisherId));
	
	publisherEntity = modelMapper.map(publisherDTO, Publisher.class);
	publisherEntity.setPublisheID(publisherId);	
	
	publisherRepository.delete(publisherEntity);
	
	result.put("Status", 200);
	result.put("Message", "Delete Publisher Succes");
	
	return result;
	
	
	
	}
	
	
	
	
	
	
	
	

}
