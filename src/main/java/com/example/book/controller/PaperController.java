package com.example.book.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.ModelDTO.PaperDTO;
import com.example.book.exception.ResourceNotFoundException;
import com.example.book.model.Paper;
import com.example.book.repositories.PaperRepository;

@RestController
@RequestMapping("/api")
public class PaperController {
	

	ModelMapper modelMapper = new ModelMapper();
	//PaperDTO paperDTO = modelMapper.map(Paper, PaperDTO.class);

	
	@Autowired
	PaperRepository paperRepository;
	
	
//	@GetMapping("/paper/readAll")
//	public List<Paper>getAllPapers(){
//		return paperRepository.findAll();
//	}
		
	
	
	@GetMapping("/paper/readDTO")
	public HashMap<String, Object>getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari paper dg menggunakan method find all dr repos
		ArrayList<Paper> listPaperEntity = (ArrayList<Paper>) paperRepository.findAll();
		
		//Buat sebuah arraylistDTO
		ArrayList<PaperDTO> listPaperDTO = new ArrayList<PaperDTO>();
		
		//Maping semua object dari entity ke DTO menggunakan Loooping
				for(Paper paper : listPaperEntity) {
					//Inisailisai DTO
					//PaperDTO paperDTO = new PaperDTO();
					PaperDTO paperDTO = modelMapper.map(paper, PaperDTO.class);
					//Maping nilai dr entity ke dto
//					paperDTO.setPaperID(paper.getPaperID());
//					paperDTO.setPaperPrice(paper.getPaperPrice());
//					paperDTO.setQualityName(paper.getQualityName());
					
					//memasukan object publisher DTO ke arraylist
					listPaperDTO.add(paperDTO);
				
				}
				
				result.put("Status", 200);
				result.put("Message", "Read all Paper data succes");
				result.put("Data", listPaperDTO);
				
				return result;
	
	
	}
	
	@GetMapping("/paper/read")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Paper> listPaperEntity = (ArrayList<Paper>) paperRepository.findAll();
		ArrayList<PaperDTO> listPaperDTO = new ArrayList<PaperDTO>();
		for(Paper paper : listPaperEntity) {
			PaperDTO paperDTO = modelMapper.map(paper, PaperDTO.class);
			listPaperDTO.add(paperDTO);
			
		}
		
		result.put("Status", 200);
		result.put("Message", "Read all Paper data succes");
		result.put("Data", listPaperDTO);
		
		return result;
		
	}

	
	
//	@PostMapping("/paper/create")
//	public Paper createPaper(@Valid @RequestBody Paper paper) {
//		return paperRepository.save(paper);
//	}
	
	
	@PostMapping("/paper/createDTO")
	public HashMap<String, Object> createPaperDTO(@Valid @RequestBody PaperDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//buat Sebuah Object Paper entity
		Paper paperEntity = new Paper();
		
		paperEntity.setQualityName(body.getQualityName());
		paperEntity.setPaperPrice(body.getPaperPrice());
		
		//ini save data ke database
		paperRepository.save(paperEntity);
		body.setPaperID(paperEntity.getPaperID());
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Create New Paper Succes");
		result.put("Data", body);
		
		
		return result;
	}
	
//	@PostMapping("/paper/create")
//	public HashMap<String, Object> createPaperDTOMaping(@Valid @RequestBody PaperDTO body){
//		HashMap<String, Object> result = new HashMap<String, Object>();
//		
//		ModelMapper modelMapper = new ModelMapper();
//		Paper paperEntity = modelMapper.map(body, Paper.class);
//		paperRepository.save(paperEntity);
//		
//		
//		//Maping result untuk respon API
//		result.put("Status", 200);
//		result.put("Message", "Create New Paper Succes");
//		result.put("Data", body);
//		
//		return result;
//
//	
//	}
	
	@PostMapping("/paper/create")
	public HashMap<String, Object> createPaperDTOMaping(@Valid @RequestBody PaperDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Paper paperEntity = modelMapper.map(body, Paper.class);
		paperRepository.save(paperEntity);
		
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Create New Paper Succes");
		result.put("Data", body);
		
		return result;

	
	}
	
	

	
	
	
	
	@GetMapping("/paper/{id}")
	public Paper getPaperById(@PathVariable(value = "id") Long paperId) {
		
		return paperRepository.findById(paperId)
				.orElseThrow(() -> new ResourceNotFoundException ("Paper", "id", paperId));
		
	}
	
	
//	@PutMapping("/paper/update/{id}")
//	public Paper updatePaper(@PathVariable(value = "id") Long paperId,
//            @Valid @RequestBody Paper paperDetails) {
//		
//		
//		Paper paper = paperRepository.findById(paperId)
//				.orElseThrow(() -> new ResourceNotFoundException ("Paper", "id", paperId));
//		
//		paper.setPaperPrice(paperDetails.getPaperPrice());
//		paper.setQualityName(paperDetails.getQualityName());
//		
//		
//		Paper updatedPaper = paperRepository.save(paper);
//		return updatedPaper;
//	
//	}
	
	
	@PutMapping("/paper/updateDTO/{id}")
	public HashMap<String, Object> updatePaperDTO(@PathVariable(value = "id") Long paperId,
            @Valid @RequestBody PaperDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//buat Sebuah Object Paper entity
		Paper paperEntity = paperRepository.findById(paperId)
				.orElseThrow(() -> new ResourceNotFoundException ("Paper", "id", paperId));
		
		paperEntity.setQualityName(body.getQualityName());
		paperEntity.setPaperPrice(body.getPaperPrice());
		
		//ini save data ke database
		paperRepository.save(paperEntity);
		
		body.setPaperID(paperEntity.getPaperID());
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Update Paper Succes");
		result.put("Data", body);
		
		
		return result;
	}
	
	@PutMapping("/paper/update/{id}")
	public HashMap<String, Object> updatePaperDTOMaping(@PathVariable(value = "id") Long paperId,
            @Valid @RequestBody PaperDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		//buat Sebuah Object Paper entity
				Paper paperEntity = paperRepository.findById(paperId)
						.orElseThrow(() -> new ResourceNotFoundException ("Paper", "id", paperId));
		
		paperEntity = modelMapper.map(body, Paper.class);
		paperEntity.setPaperID(paperId);
		
		paperRepository.save(paperEntity);
		body =  modelMapper.map(paperEntity, PaperDTO.class);
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Update Paper Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	
	
//	@DeleteMapping("/paper/delete/{id}")
//	public ResponseEntity<?> deletePaper(@PathVariable(value = "id") Long paperId){
//		
//		Paper paper = paperRepository.findById(paperId)
//				.orElseThrow(() -> new ResourceNotFoundException ("Paper", "id", paperId));
//		
//		paperRepository.delete(paper);
//		
//		return ResponseEntity.ok().build();
//
//	}
	
	@DeleteMapping("/paper/deleteDTO/{id}")
	public HashMap<String, Object> deletePaperDTO(@PathVariable(value = "id") Long paperId,
			PaperDTO paperDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//buat Sebuah Object Paper entity
		Paper paperEntity = paperRepository.findById(paperId)
				.orElseThrow(() -> new ResourceNotFoundException ("Paper", "id", paperId));
		
		paperEntity.setQualityName(paperDTO.getQualityName());
		paperEntity.setPaperPrice(paperDTO.getPaperPrice());
		
		//Proses delete
		paperRepository.delete(paperEntity);
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Delete Paper Succes");
		
		
		
		
		return result;
	
	}
	
	@DeleteMapping("/paper/delete/{id}")
	public HashMap<String, Object> deletePaperDTOMaping(@PathVariable(value = "id") Long paperId,
			PaperDTO paperDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//buat Sebuah Object Paper entity
		Paper paperEntity = paperRepository.findById(paperId)
				.orElseThrow(() -> new ResourceNotFoundException ("Paper", "id", paperId));
		
		paperEntity = modelMapper.map(paperDTO, Paper.class);
		paperEntity.setPaperID(paperId);
		
		paperRepository.delete(paperEntity);
		
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Delete Paper Succes");
		
		return result;
		
	}

	
	
	
	
	
	
	

}
