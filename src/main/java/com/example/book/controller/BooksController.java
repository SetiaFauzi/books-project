package com.example.book.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.ModelDTO.AuthorDTO;
import com.example.book.ModelDTO.BooksDTO;
import com.example.book.ModelDTO.PaperDTO;
import com.example.book.ModelDTO.PublisherDTO;
import com.example.book.exception.ResourceNotFoundException;
import com.example.book.model.Author;
import com.example.book.model.Books;
import com.example.book.model.Publisher;
import com.example.book.repositories.BooksRepository;


@RestController
@RequestMapping("/api")
public class BooksController {
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	BooksRepository booksRepository;
	
	
//	@GetMapping("/books/readAll")
//	public List<Books>getAllBooks(){
//		return booksRepository.findAll();
//		
//	}
	
//	@GetMapping("/books/readDTO")
//	public HashMap<String, Object>getAllData(){
//		HashMap<String, Object> result = new HashMap<String, Object>();
//		
//		//Ambil semua data dari auth dg menggunakan method find all dr repos
//		ArrayList<Books> listBooksEntity = (ArrayList<Books>) booksRepository.findAll();
//		//Buat sebuah arraylistDTO
//		ArrayList<BooksDTO> listBooksDTO = new ArrayList<BooksDTO>();
//		
//		//Maping semua object dari entity ke DTO menggunakan Loooping
//		for(Books books : listBooksEntity) {
//			//Inisailisai DTO
//			BooksDTO booksDTO = new BooksDTO();
//			//inis obj Author dto
//			AuthorDTO authorDTO = new AuthorDTO();
//			//inis obj Publisher dto
//			PublisherDTO publisherDTO = new PublisherDTO();
//			//inis obj paper dto
//			PaperDTO paperDTO = new PaperDTO();
//			
//			//Maping SEMUA nilai dr entity ke dto
//			
//			paperDTO.setPaperID(books.getPublisher().getPaperQuality().getPaperID());
//			paperDTO.setPaperPrice(books.getPublisher().getPaperQuality().getPaperPrice());
//			paperDTO.setQualityName(books.getPublisher().getPaperQuality().getQualityName());
//			
//			authorDTO.setAuthorID(books.getAuthor().getAuthorID());
//			authorDTO.setFirstName(books.getAuthor().getFirstName());
//			authorDTO.setLastName(books.getAuthor().getLastName());
//			authorDTO.setAge(books.getAuthor().getAge());
//			authorDTO.setCountry(books.getAuthor().getCountry());
//			authorDTO.setGender(books.getAuthor().getGender());
////			authorDTO.setRating(books.getAuthor().getRating());
//			
//			publisherDTO.setPublisheID(books.getPublisher().getPublisheID());
//			publisherDTO.setCompanyName(books.getPublisher().getCompanyName());
//			publisherDTO.setCountry(books.getPublisher().getCountry());
//			publisherDTO.setPaperQuality(paperDTO);
//			
//			//Maping nilai dr entity kr DTO
//			booksDTO.setBookID(books.getBookID());
//			booksDTO.setTitle(books.getTitle());
//			booksDTO.setReleaseDate(books.getReleaseDate());
//			booksDTO.setAuthor_map(authorDTO);
//			booksDTO.setPublisher_map(publisherDTO);
//			
//			//memasukan object publisher DTO ke arraylist
//			listBooksDTO.add(booksDTO);
//
//		}
//		result.put("Status", 200);
//		result.put("Message", "Read all Books data succes");
//		result.put("Data", listBooksDTO);
//		
//		return result;
//	
//		
//	}
	
	@GetMapping("/books/read")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Books> listBooksEntity = (ArrayList<Books>) booksRepository.findAll();
		ArrayList<BooksDTO> listBooksDTO = new ArrayList<BooksDTO>();
		
		for(Books books : listBooksEntity) {
			BooksDTO booksDTO = modelMapper.map(books, BooksDTO.class);
			listBooksDTO.add(booksDTO);
			
		}
		
		result.put("Status", 200);
		result.put("Message", "Read all Books data succes");
		result.put("Data", listBooksDTO);
		
		return result;
	
	}
	
	
	
//	@PostMapping("/books/create")
//	public Books createboBooks(@Valid @RequestBody Books books) {
//		return booksRepository.save(books);
//	}
	
//	@PostMapping("/books/createDTO")
//	public HashMap<String, Object> createBooksDTO(@Valid @RequestBody BooksDTO body){
//		HashMap<String, Object> result = new HashMap<String, Object>();
//	
//	//buat Sebuah Object books ke entity
//	Books booksEntity = new Books();
//	
//	//buat Sebuah Object Publisher entity
//	Publisher publisherEntity = new Publisher();
//	//Paper paperEntity = new Paper();
//	//buat Sebuah Object author entity
//	Author authorEntity = new Author();
//	
//	//inisialisasi obj paper entity
//	authorEntity.setAuthorID(body.getAuthor_map().getAuthorID());
//	publisherEntity.setPublisheID(body.getPublisher_map().getPublisheID());
//	
//	//Maping value dari Book DTO ke entity
//	booksEntity.setTitle(body.getTitle());
//	booksEntity.setPrice(body.getPrice());
//	booksEntity.setReleaseDate(body.getReleaseDate());
//	booksEntity.setAuthor(authorEntity);
//	booksEntity.setPublisher(publisherEntity);
//	
//	//ini save data ke database
//	booksRepository.save(booksEntity);
//	body.setBookID(booksEntity.getBookID());
//	
//	//Maping result untuk respon API
//	result.put("Status", 200);
//	result.put("Message", "Create New Books Succes");
//	result.put("Data", body);
//	
//	return result;
//		
//		
//		
//	}
	
//	@PostMapping("/books/create")
//	public HashMap<String, Object> createBooksDTOMaping(@Valid @RequestBody BooksDTO body){
//		HashMap<String, Object> result = new HashMap<String, Object>();
//	
//		ModelMapper modelMapper = new ModelMapper();
//		Books booksEntity = modelMapper.map(body, Books.class);
//		
//		BooksDTO booksDTO = modelMapper.map(body, BooksDTO.class);
//		
//		//booksEntity.setPrice(calculateBooksPrice(body.getPublisher_map().getPublisheID(), body.getAuthor_map().getAuthorID()));
//
//		booksDTO.setPrice(calculateBooksPrice(body.getPublisher_map().getPublisheID(), body.getAuthor_map().getAuthorID()));
//		
//		
//		booksRepository.save(booksEntity);
//		body.setBookID(booksEntity.getBookID());
//		body.setPrice(booksEntity.getPrice());
//		
//		result.put("Status", 200);
//		result.put("Message", "Create New Books Succes");
//		result.put("Data", body);
//		
//		return result;
//	
//	}
	
	public BigDecimal calculateBooksPrice(Long publisherId, Long authorId) {
		BigDecimal paperPrice = new BigDecimal(0);
		BigDecimal ratePrice = new BigDecimal(0);
		BigDecimal rateBookPrice = new BigDecimal(1.1);
		
		ArrayList<Books> listBooksEntity = (ArrayList<Books>) booksRepository.findAll();
		
		for(Books books : listBooksEntity) {
			if((books.getPublisher().getPublisheID() == publisherId) && (books.getAuthor().getAuthorID() == authorId)) {
				paperPrice = books.getPublisher().getPaperQuality().getPaperPrice();
				ratePrice = books.getAuthor().getRating().getRatePrice();
			
				
			}
		}
		BigDecimal bookPrice = paperPrice.multiply(ratePrice).multiply(rateBookPrice);
		
		return bookPrice;
	}
	
	
	
	@PostMapping("/books/create")
	public HashMap<String, Object> createBooksDtoMaping(@Valid @RequestBody BooksDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();

		ModelMapper modelMapper = new ModelMapper();
		Books booksEntity = modelMapper.map(body, Books.class);
		Author authorEntity = modelMapper.map(body, Author.class);
		Publisher publisherEntity = modelMapper.map(body, Publisher.class);
		
		
		authorEntity.setAuthorID(body.getAuthor_map().getAuthorID());
		publisherEntity.setPublisheID(body.getPublisher_map().getPublisheID());
	
		booksEntity.setPrice(calculateBooksPrice(body.getPublisher_map().getPublisheID(),
				body.getAuthor_map().getAuthorID()));
		booksRepository.save(booksEntity);
		body.setBookID(booksEntity.getBookID());
		
		result.put("Status", 200);
		result.put("Message", "Create New Books Succes");
		result.put("Data", body);
		
		return result;
	
	}
	

	
	@PutMapping("/books/updateDTO/{id}")
	public HashMap<String, Object> updateBooksDTO(@PathVariable(value = "id") Long bookID,
            @Valid @RequestBody BooksDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//buat Sebuah Object Books entity
		Books booksEntity = booksRepository.findById(bookID)
						.orElseThrow(() -> new ResourceNotFoundException ("Books", "id", bookID));
	
		booksEntity.setTitle(body.getTitle());
		booksEntity.setPrice(body.getPrice());
		booksEntity.setReleaseDate(body.getReleaseDate());
		
		//ini save data ke database
		booksRepository.save(booksEntity);
		body.setBookID(booksEntity.getBookID());
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Update one of a Books is Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@PutMapping("/books/update/{id}")
	public HashMap<String, Object> updateBooksDTOMaping(@PathVariable(value = "id") Long bookID,
            @Valid @RequestBody BooksDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
	
		Books booksEntity = booksRepository.findById(bookID)
				.orElseThrow(() -> new ResourceNotFoundException ("Books", "id", bookID));
		
		booksEntity = modelMapper.map(body, Books.class);
		booksEntity.setBookID(bookID);
		
		Author authorEntity = modelMapper.map(body, Author.class);
		Publisher publisherEntity = modelMapper.map(body, Publisher.class);
		authorEntity.setAuthorID(body.getAuthor_map().getAuthorID());
		publisherEntity.setPublisheID(body.getPublisher_map().getPublisheID());
		
		booksRepository.save(booksEntity);
		body = modelMapper.map(booksEntity, BooksDTO.class);
		body.setBookID(booksEntity.getBookID());
		
		result.put("Status", 200);
		result.put("Message", "Update one of a Books is Succes");
		result.put("Data", body);
		
		return result;
	
	
	
	}
	
	
	
	
	
	@DeleteMapping("/books/deleteDTO/{id}")
	public HashMap<String, Object> deleteeBooksDTO(@PathVariable(value = "id") Long bookID,
            BooksDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//buat Sebuah Object Books entity
		Books booksEntity = booksRepository.findById(bookID)
				.orElseThrow(() -> new ResourceNotFoundException ("Books", "id", bookID));
		
		booksEntity.setTitle(body.getTitle());
		booksEntity.setPrice(body.getPrice());
		booksEntity.setReleaseDate(body.getReleaseDate());
		
		//Proses Delete
		booksRepository.delete(booksEntity);
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Delete one of a Books is Succes");
		
		return result;
	}
	
	@DeleteMapping("/books/delete/{id}")
	public HashMap<String, Object> deleteBooksDTOMaping(@PathVariable(value = "id") Long bookID,
            BooksDTO booksDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Books booksEntity = booksRepository.findById(bookID)
				.orElseThrow(() -> new ResourceNotFoundException ("Books", "id", bookID));
		
		booksEntity = modelMapper.map(booksDTO, Books.class);
		booksEntity.setBookID(bookID);
		
		booksRepository.delete(booksEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete one of a Books is Succes");
		
		return result;
		
	}

	

	
	
	
}
