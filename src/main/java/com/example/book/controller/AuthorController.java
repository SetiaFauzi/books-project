package com.example.book.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.ModelDTO.AuthorDTO;
import com.example.book.ModelDTO.PaperDTO;
import com.example.book.exception.ResourceNotFoundException;
import com.example.book.model.Author;
import com.example.book.repositories.AuthorRepository;
import com.example.book.repositories.PaperRepository;


@RestController
@RequestMapping("/api")
public class AuthorController {

	@Autowired
	AuthorRepository authorRepository;
	
	
//	@GetMapping("/author/readAll")
//	public List<Author>getAllAuthor(){
//		return authorRepository.findAll();
//		
//	}
	
	@GetMapping("/author/readDTO")
	public HashMap<String, Object>getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
	//Ambil semua data dari auth dg menggunakan method find all dr repos
	ArrayList<Author> listAuthorEntity = (ArrayList<Author>) authorRepository.findAll();
	
	//Buat sebuah arraylistDTO
	ArrayList<AuthorDTO> listAuthorDTO = new ArrayList<AuthorDTO>();
	
	//Maping semua object dari entity ke DTO menggunakan Loooping
	for(Author author : listAuthorEntity) {
		
		//Inisailisai DTO
		AuthorDTO authorDTO = new AuthorDTO();
		
		//Maping nilai dr entity ke dto
		authorDTO.setAuthorID(author.getAuthorID());
		authorDTO.setFirstName(author.getFirstName());
		authorDTO.setLastName(author.getLastName());
		authorDTO.setAge(author.getAge());
		authorDTO.setCountry(author.getCountry());
		authorDTO.setGender(author.getGender());
		
		
	//	authorDTO.setRating(author.getRating());
		
		//memasukan object publisher DTO ke arraylist
		listAuthorDTO.add(authorDTO);
		
	}
	
	result.put("Status", 200);
	result.put("Message", "Read all Author data succes");
	result.put("Data", listAuthorDTO);
	
	return result;
	
	
	}
	
	@GetMapping("/author/read")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		
	ArrayList<Author> listAuthorEntity = (ArrayList<Author>) authorRepository.findAll();
	ArrayList<AuthorDTO> listAuthorDTO = new ArrayList<AuthorDTO>();
	
	for(Author author : listAuthorEntity) {
		AuthorDTO authorDTO = modelMapper.map(author, AuthorDTO.class);
		listAuthorDTO.add(authorDTO);
	}
	
	result.put("Status", 200);
	result.put("Message", "Read all Author data succes");
	result.put("Data", listAuthorDTO);
	
	return result;
		
	}
	
	
	
	
	
	
	
//	@PostMapping("/author/create")
//	public Author createAuthor(@Valid @RequestBody Author author) {
//		return authorRepository.save(author);
//	}
	
	
	@PostMapping("/author/createDTO")
	public HashMap<String, Object> createAuthorDTO(@Valid @RequestBody AuthorDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
	
		
		//buat Sebuah Object author entity
		Author authorEntity = new Author();
		
		authorEntity.setFirstName(body.getFirstName());
		authorEntity.setLastName(body.getLastName());
		authorEntity.setAge(body.getAge());
		authorEntity.setGender(body.getGender());
		authorEntity.setCountry(body.getCountry());
		
		
		//authorEntity.setRating(body.getRating());
		
		//ini save data ke database
		authorRepository.save(authorEntity);
		body.setAuthorID(authorEntity.getAuthorID());
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Create New Author Succes");
		result.put("Data", body);
		
		return result;
		
		
	}
	
	@PostMapping("/author/create")
	public HashMap<String, Object> createAuthorDTOMaping(@Valid @RequestBody AuthorDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Author authorEntity = modelMapper.map(body, Author.class);
		authorRepository.save(authorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Create New Author Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	
	
	
	
	@PutMapping("/author/updateDTO/{id}")
	public HashMap<String, Object> updateAuthorDTO(@PathVariable(value = "id") Long authorID,
            @Valid @RequestBody AuthorDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
	
		//buat Sebuah Object Author entity
		Author authorEntity = authorRepository.findById(authorID)
				.orElseThrow(() -> new ResourceNotFoundException ("Author", "id", authorID));
		
		authorEntity.setFirstName(body.getFirstName());
		authorEntity.setLastName(body.getLastName());
		authorEntity.setAge(body.getAge());
		authorEntity.setGender(body.getGender());
		authorEntity.setCountry(body.getCountry());
		
		
		//authorEntity.setRating(body.getRating());
		
		//ini save data ke database
		authorRepository.save(authorEntity);
		body.setAuthorID(authorEntity.getAuthorID());
		
		//Maping result untuk respon API
		result.put("Status", 200);
		result.put("Message", "Update Author Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@PutMapping("/author/update/{id}")
	public HashMap<String, Object> updateAuthorDTOMaping(@PathVariable(value = "id") Long authorID,
            @Valid @RequestBody AuthorDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Author authorEntity = authorRepository.findById(authorID)
				.orElseThrow(() -> new ResourceNotFoundException ("Author", "id", authorID));
		
		authorEntity = modelMapper.map(body, Author.class);
		authorEntity.setAuthorID(authorID);
		
		authorRepository.save(authorEntity);
		body = modelMapper.map(authorEntity, AuthorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Author Succes");
		result.put("Data", body);
		
		return result;
	}
	
	
	
	
	
	
	@DeleteMapping("/author/deleteDTO/{id}")
	public HashMap<String, Object> deleteAuthorDTO(@PathVariable(value = "id") Long authorID,
           AuthorDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
	//buat Sebuah Object Author entity
	Author authorEntity = authorRepository.findById(authorID)
			.orElseThrow(() -> new ResourceNotFoundException ("Author", "id", authorID));
	
	authorEntity.setFirstName(body.getFirstName());
	authorEntity.setLastName(body.getLastName());
	authorEntity.setAge(body.getAge());
	authorEntity.setGender(body.getGender());
	authorEntity.setCountry(body.getCountry());
	
	
	//authorEntity.setRating(body.getRating());
	
	//Proses delete
	authorRepository.delete(authorEntity);
	
	//Maping result untuk respon API
	result.put("Status", 200);
	result.put("Message", "Delete One of Author Succes");
	
	return result;
		

	}
	
	@DeleteMapping("/author/delete/{id}")
	public HashMap<String, Object> deleteAuthorDTOMaping(@PathVariable(value = "id") Long authorID,
			AuthorDTO authorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
	
		Author authorEntity = authorRepository.findById(authorID)
				.orElseThrow(() -> new ResourceNotFoundException ("Author", "id", authorID));	
		
		authorEntity = modelMapper.map(authorDTO, Author.class);
		authorEntity.setAuthorID(authorID);
		
		authorRepository.delete(authorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete One of Author Succes");
		
		return result;
	}
	
	
	
}
