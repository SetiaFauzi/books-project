package com.example.book.ModelDTO;

import java.math.BigDecimal;

public class RatingDTO {
	
	private Long ratingID;
	private String rating;
	private BigDecimal ratePrice;
	
	public RatingDTO() {
		super();
	}

	public RatingDTO(Long ratingID, String rating, BigDecimal ratePrice) {
		super();
		this.ratingID = ratingID;
		this.rating = rating;
		this.ratePrice = ratePrice;
	}

	public Long getRatingID() {
		return ratingID;
	}

	public void setRatingID(Long ratingID) {
		this.ratingID = ratingID;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public BigDecimal getRatePrice() {
		return ratePrice;
	}

	public void setRatePrice(BigDecimal ratePrice) {
		this.ratePrice = ratePrice;
	}

	
	
	
	
	

}
