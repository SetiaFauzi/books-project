package com.example.book.ModelDTO;

public class AuthorDTO {
	
	private Long authorID;
	private String firstName;
	private String lastName;
	private String gender;
	private Integer age;
	private String country;
	private RatingDTO rating;
	
	
	public AuthorDTO() {
		super();
	}


	public AuthorDTO(Long authorID, String firstName, String lastName, String gender, Integer age, String country,
			RatingDTO rating) {
		super();
		this.authorID = authorID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.age = age;
		this.country = country;
		this.rating = rating;
	}


	public Long getAuthorID() {
		return authorID;
	}


	public void setAuthorID(Long authorID) {
		this.authorID = authorID;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public Integer getAge() {
		return age;
	}


	public void setAge(Integer age) {
		this.age = age;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public RatingDTO getRating() {
		return rating;
	}


	public void setRating(RatingDTO rating) {
		this.rating = rating;
	}
	
	
	
	
	
	
	
	
	
	
	

}
