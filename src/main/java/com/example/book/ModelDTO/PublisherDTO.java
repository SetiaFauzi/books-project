package com.example.book.ModelDTO;

public class PublisherDTO {
	
	private Long publisheID;
	private String companyName;
	private String country;
	private PaperDTO paperQuality;
	
	public PublisherDTO() {
		super();
	}


	public PublisherDTO(Long publisheID, String companyName, String country, PaperDTO paperQuality) {
		super();
		this.publisheID = publisheID;
		this.companyName = companyName;
		this.country = country;
		this.paperQuality = paperQuality;
	}


	public Long getPublisheID() {
		return publisheID;
	}


	public void setPublisheID(Long publisheID) {
		this.publisheID = publisheID;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public PaperDTO getPaperQuality() {
		return paperQuality;
	}


	public void setPaperQuality(PaperDTO paperQuality) {
		this.paperQuality = paperQuality;
	}



	
	
	
	
	
	

}
