package com.example.book.ModelDTO;

import java.math.BigDecimal;
import java.util.Date;

import com.example.book.model.Author;
import com.example.book.model.Publisher;

public class BooksDTO {
	
	private Long bookID;
	private String title;
	private Date releaseDate;
	private BigDecimal price;
	private AuthorDTO author_map;
	private PublisherDTO publisher_map;
	 
	 
	public BooksDTO() {
		super();
	}


	public BooksDTO(Long bookID, String title, Date releaseDate, BigDecimal price, AuthorDTO author_map,
			PublisherDTO publisher_map) {
		super();
		this.bookID = bookID;
		this.title = title;
		this.releaseDate = releaseDate;
		this.price = price;
		this.author_map = author_map;
		this.publisher_map = publisher_map;
	}


	public Long getBookID() {
		return bookID;
	}


	public void setBookID(Long bookID) {
		this.bookID = bookID;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Date getReleaseDate() {
		return releaseDate;
	}


	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}


	public BigDecimal getPrice() {
		return price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public AuthorDTO getAuthor_map() {
		return author_map;
	}


	public void setAuthor_map(AuthorDTO author_map) {
		this.author_map = author_map;
	}


	public PublisherDTO getPublisher_map() {
		return publisher_map;
	}


	public void setPublisher_map(PublisherDTO publisher_map) {
		this.publisher_map = publisher_map;
	}

	 
	 
	 
}
