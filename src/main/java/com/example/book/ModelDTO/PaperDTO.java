package com.example.book.ModelDTO;

import java.math.BigDecimal;

public class PaperDTO {

	private Long paperID;
	private String qualityName;
	private BigDecimal paperPrice;
	
	
	public PaperDTO() {

	}
	
	public PaperDTO(Long paperID, String qualityName, BigDecimal paperPrice) {
		super();
		this.paperID = paperID;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
	}


	public Long getPaperID() {
		return paperID;
	}
	public void setPaperID(Long paperID) {
		this.paperID = paperID;
	}
	public String getQualityName() {
		return qualityName;
	}
	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}
	public BigDecimal getPaperPrice() {
		return paperPrice;
	}
	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}
	
	
	
	
	
	
}
