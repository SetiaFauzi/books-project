package com.example.book.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@Table(name = "publisher")
@EntityListeners(AuditingEntityListener.class)
public class Publisher implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long publisheID;
	
	@NotBlank
	private String companyName;
	
	@NotBlank
	private String country;
	
	 
	 
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paper_id")
    private Paper paperQuality;
	
	@OneToMany(
	        mappedBy = "publisher_map",
	        cascade = CascadeType.PERSIST,
	        fetch = FetchType.LAZY
	    )
	 
	private Set<Books> books;


	public Publisher() {
		books = new HashSet<>();
	}
	
	public Publisher(@NotBlank String companyName, @NotBlank String country) {
		super();
		this.companyName = companyName;
		this.country = country;
		books = new HashSet<>();
	}





	public Long getPublisheID() {
		return publisheID;
	}


	public void setPublisheID(Long publisheID) {
		this.publisheID = publisheID;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}



	public Paper getPaperQuality() {
		return paperQuality;
	}


	public void setPaperQuality(Paper paperQuality) {
		this.paperQuality = paperQuality;
		paperQuality.getPublishers().add(this);
	}


	public Set<Books> getBooks() {
		return books;
	}


	public void setBooks(Set<Books> books) {
		this.books = books;
		for(Books books2 : books) {
			books2.setPublisher(this);
		}
	}

	

	

}
