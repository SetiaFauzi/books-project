package com.example.book.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Author")
public class Author implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long authorID;
	
	@Column(nullable = false)
	private String firstName;
	
	@Column(nullable = false)
	private String lastName;
	
	@Column(nullable = false)
	private String gender;
	
	@Column(nullable = false)
	private Integer age;
	
	@Column(nullable = false)
	private String country;
	
	@Column(nullable = false)
	private Rating rating;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rating_id")
    private Rating rating_map;
	 
	 
	@OneToMany(
	        mappedBy = "author_map",
	        cascade = CascadeType.PERSIST,
	        fetch = FetchType.LAZY
	    )
	 
	private Set<Books> books;
	
	 
	public Author() {
		books = new HashSet<>();
	}

	
	public Author(String firstName, String lastName, String gender, Integer age, String country, Rating rating) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.age = age;
		this.country = country;
		this.rating = rating;
		books = new HashSet<>();
	}


	public Long getAuthorID() {
		return authorID;
	}
	public void setAuthorID(Long authorID) {
		this.authorID = authorID;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Rating getRating() {
		return rating;
	}
	public void setRating(Rating rating) {
		this.rating = rating;
	}
	public Set<Books> getBooks() {
		return books;
	}
	
	public Rating getRating_map() {
		return rating_map;
	}


	public void setRating_map(Rating rating_map) {
		this.rating_map = rating_map;
		rating_map.getAuthors().add(this);
	}


	public void setBooks(Set<Books> books) {
		this.books = books;
		for (Books books2 : books) {
		books2.setAuthor(this);
		}
	}
	
	
	
	

}
