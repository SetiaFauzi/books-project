package com.example.book.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "BooksTable")
public class Books implements Serializable{

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long bookID;
	
	@Column(nullable = false)
	private String title;
	
	@JsonFormat(pattern = "yyyy-mm-dd")
	@Column(nullable = false)
	private Date releaseDate;
	
	
	@Column(nullable = false)
	private BigDecimal price;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private Author author_map;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "publisher_id")
    private Publisher publisher_map;

	
	public Books() {
		super();
	}
	



	public Long getBookID() {
		return bookID;
	}


	public void setBookID(Long bookID) {
		this.bookID = bookID;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Date getReleaseDate() {
		return releaseDate;
	}


	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}


	public BigDecimal getPrice() {
		return price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public Author getAuthor() {
		return author_map;
	}


	public void setAuthor(Author author) {
		this.author_map = author;
		author.getBooks().add(this);
	}


	public Publisher getPublisher() {
		return publisher_map;
	}


	public void setPublisher(Publisher publisher) {
		this.publisher_map = publisher;
		publisher.getBooks().add(this);
		
	}
	
	
	
	
}
