package com.example.book.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Rating")
public class Rating implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long ratingID;
	
	@Column(nullable = false)
	private String rating;
	
	@Column(nullable = false)
	private BigDecimal ratePrice;
	
	
	@OneToMany(
	        mappedBy = "rating_map",
	        cascade = CascadeType.PERSIST,
	        fetch = FetchType.LAZY
	    )
	 
	private Set<Author> authors;
	

	public Rating() {
		super();
		authors= new HashSet<>();
	}

	
	

	public Rating(Long ratingID, String rating, BigDecimal ratePrice) {
		super();
		this.ratingID = ratingID;
		this.rating = rating;
		this.ratePrice = ratePrice;
		authors= new HashSet<>();
	}

	public Long getRatingID() {
		return ratingID;
	}

	public void setRatingID(Long ratingID) {
		this.ratingID = ratingID;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public BigDecimal getRatePrice() {
		return ratePrice;
	}

	public void setRatePrice(BigDecimal ratePrice) {
		this.ratePrice = ratePrice;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
		for(Author authorlist : authors) {
			authorlist.setRating_map(this);
		}
	}
	
	
	
	

}
