package com.example.book.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.book.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

	
	
}
