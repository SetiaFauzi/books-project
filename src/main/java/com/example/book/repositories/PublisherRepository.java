package com.example.book.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.book.model.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {
	
	
}
