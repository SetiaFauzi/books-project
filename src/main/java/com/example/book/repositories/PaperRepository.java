package com.example.book.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.book.model.Paper;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {

}
