package com.example.book.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.book.model.Rating;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

	
}
